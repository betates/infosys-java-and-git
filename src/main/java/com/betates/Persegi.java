package main.java.com.betates;

public class Persegi {
    private int panjang;
    private int tinggi;

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
    }

    public Persegi(int panjang, int tinggi){
        this.panjang = panjang;
        this.tinggi = tinggi;
    }

    public int volume(int p, int t) {
        return p*t;
    }



}