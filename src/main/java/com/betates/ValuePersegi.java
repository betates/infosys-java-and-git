package main.java.com.betates;

public class ValuePersegi extends Persegi{
    public ValuePersegi(int panjang, int tinggi) {
        super(panjang, tinggi);
    }

    @Override
    public int getPanjang() {
        return super.getPanjang();
    }

    @Override
    public void setPanjang(int panjang) {
        super.setPanjang(panjang);
    }

    @Override
    public int getTinggi() {
        return super.getTinggi();
    }

    @Override
    public void setTinggi(int tinggi) {
        super.setTinggi(tinggi);
    }

    public static void main(String[] args) {
        ValuePersegi persegi = new ValuePersegi(20, 20);
        persegi.setPanjang(50);
        System.out.println(persegi.getPanjang());
        persegi.setTinggi(50);
        System.out.println(persegi.getTinggi());
    }
}
