# Java
Class with Attribute
```java
public class Persegi {
    int panjang = 10;
    int tinggi = 10;
}
```
Method
```java
...
    public int volume(int p, int t) {
        return p*t;
    }
...
```
Constructor
```java
    public Persegi(int panjang, int tinggi) {
        this.panjang = panjang;
        this.tinggi = tinggi;
    }
```
Getter and Setter
```java
    ...

    private int panjang;
    private int tinggi;
    
    ...

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
    }
    
    ...
```

Inheritance
```java
public class ValuePersegi extends Persegi{
    public ValuePersegi(int panjang, int tinggi) {
        super(panjang, tinggi);
    }

    @Override
    public int getPanjang() {
        return super.getPanjang();
    }

    @Override
    public void setPanjang(int panjang) {
        super.setPanjang(panjang);
    }

    @Override
    public int getTinggi() {
        return super.getTinggi();
    }

    @Override
    public void setTinggi(int tinggi) {
        super.setTinggi(tinggi);
    }

    public static void main(String[] args) {
        ValuePersegi persegi = new ValuePersegi(20, 20);
        persegi.setPanjang(50);
        System.out.println(persegi.getPanjang());
        persegi.setTinggi(50);
        System.out.println(persegi.getTinggi());
    }
}
```
Interface
```java
public interface Employee {
    public void id();
    public void name();
}
```
Implements
```java
public class Emp1 implements Employee{
    @Override
    public void id() {
        System.out.println(1);
    }

    @Override
    public void name() {
        System.out.println("Rofiq");
    }
}
```
# Git
Cloning Repository<br>
![Screenshot1](screenshot/clone-project.png)<br><br>